{
"mutant_slime": {
"ID": "mutant_slime",
"growth_speed": "1",
"growth_thresholds": "10,25",
"icon": "mutant_slime",
"mature": "IF:is_class,slime
force_tokens,impregnation_block,preg_second
alts,preg,pregnant
ELSE:
force_tokens,preg_third
alts,preg
ENDIF",
"name": "Mutant Slime Offspring",
"normal": "IF:is_class,slime
force_tokens,impregnation_block,preg_second
alts,preg,pregnant
ELSE:
force_tokens,preg_third
alts,preg
ENDIF
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "5,50,200",
"young": "IF:is_class,slime
force_tokens,impregnation_block,preg_second
alts,preg,pregnant
ELSE:
force_tokens,preg_third
alts,preg
ENDIF
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"mutant_vine": {
"ID": "mutant_vine",
"growth_speed": "0",
"growth_thresholds": "0,1",
"icon": "mutant_vine",
"mature": "force_tokens,impregnation_block,preg_second
alts,preg
loveREC,50",
"name": "Mutant Vine",
"normal": "force_tokens,impregnation_block,preg_second
alts,preg
loveREC,50",
"value": "50,50,50",
"young": "force_tokens,impregnation_block,preg_second
alts,preg"
},
"parasite": {
"ID": "parasite",
"growth_speed": "1",
"growth_thresholds": "10,30",
"icon": "parasite",
"mature": "force_tokens,preg_third
alts,preg,birth,pregnant
add_moves,birth_parasite",
"name": "Parasite Offspring",
"normal": "force_tokens,preg_second
alts,preg,pregnant
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "10,20,50",
"young": "force_tokens,preg_first
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"slime": {
"ID": "slime",
"growth_speed": "1",
"growth_thresholds": "10,30",
"icon": "slime",
"mature": "force_tokens,preg_third
alts,preg,birth,pregnant
add_moves,birth_slime",
"name": "Slime Offspring",
"normal": "force_tokens,preg_second
alts,preg,pregnant
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "10,20,50",
"young": "force_tokens,preg_first
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"spider_egg": {
"ID": "spider_egg",
"growth_speed": "1",
"growth_thresholds": "0,12",
"icon": "spider_egg",
"mature": "force_tokens,preg_third
alts,preg,birth,pregnant
WHEN:turn
force_move_chance,50,lay_eggs,8",
"name": "Spider Egg",
"normal": "force_tokens,preg_second
alts,preg,pregnant
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "0,10,60",
"young": ""
},
"tentacles": {
"ID": "tentacles",
"growth_speed": "1",
"growth_thresholds": "10,20",
"icon": "tentacles",
"mature": "force_tokens,preg_third
alts,preg,birth,pregnant
add_moves,birth_tentacles",
"name": "Living Clothes Offspring",
"normal": "force_tokens,preg_second
alts,preg,pregnant
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "10,30,60",
"young": "force_tokens,preg_first
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"vine": {
"ID": "vine",
"growth_speed": "1",
"growth_thresholds": "10,30",
"icon": "vine",
"mature": "force_tokens,preg_third
alts,preg,birth,pregnant
add_moves,birth_vine
IF:has_quirk,vine_seedbed
add_moves,unbirth_vine",
"name": "Vine Offspring",
"normal": "force_tokens,preg_second
alts,preg,pregnant
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "10,20,50",
"young": "force_tokens,preg_first
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"zombie_worm": {
"ID": "zombie_worm",
"growth_speed": "1",
"growth_thresholds": "6,12",
"icon": "zombie_worm",
"mature": "force_tokens,preg_third
alts,preg,pregnant,birth
force_dot,estrus,20
loveREC,200
WHEN:turn
force_move_chance,40,wriggling_zombie_worm,10",
"name": "Zombie Worm Offspring",
"normal": "force_tokens,preg_second
alts,preg,pregnant
force_dot,estrus,10
loveREC,50
WHEN:turn
force_move_chance,20,wriggling_zombie_worm,10
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "10,20,50",
"young": "force_tokens,preg_first
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN"
},
"zombie_worm_big": {
"ID": "zombie_worm_big",
"growth_speed": "1",
"growth_thresholds": "0,30",
"icon": "zombie_worm_big",
"mature": "force_tokens,preg_third,impregnation_block
alts,preg,pregnant,birth
force_dot,estrus,20
force_dot,love,10
loveREC,200
WHEN:turn
force_move_chance,50,wriggling_zombie_worm,10",
"name": "Mutant Zombie Worm Offspring",
"normal": "force_tokens,preg_second,impregnation_block
alts,preg,pregnant
force_dot,estrus,10
loveREC,50
WHEN:turn
grow_parasite_with_modifier
ENDWHEN
WHEN:day
grow_parasite_with_modifier
ENDWHEN",
"value": "0,20,80",
"young": ""
}
}