{
"cow_emergency_discharge": {
"ID": "cow_emergency_discharge",
"crit": "0",
"from": "1,2",
"icon": "emergency_discharge",
"name": "Emergency Discharge",
"range": "1,2",
"requirements": "self_token_count,milk,2",
"script": "token_scaling,milk,100
save,WIL
tokens,blind,blind",
"selfscript": "remove_tokens,milk",
"sound": "Water1",
"to": "1,2,3,aoe",
"type": "magic",
"visual": "animation,lactate
exp,attack
projectile,StraightStream,WHITE"
},
"cow_emergency_discharge_ally": {
"ID": "cow_emergency_discharge_ally",
"crit": "",
"from": "3,4",
"icon": "emergency_discharge_ally",
"name": "Emergency Discharge",
"range": "2,3",
"requirements": "",
"script": "dot,regen,8,3
save,FOR
tokens,daze",
"selfscript": "remove_tokens,milk",
"sound": "Water1",
"to": "ally,all",
"type": "heal",
"visual": "animation,lactate
target,Rain,WHITE"
},
"cow_holy_bonk": {
"ID": "cow_holy_bonk",
"crit": "15",
"from": "1,2",
"icon": "cow_love_bonk",
"name": "Holy Bonk",
"range": "6,7",
"requirements": "",
"script": "token_scaling,milk,-20
save,WIL
move,-1
dot,love,3,2
tokens,daze",
"selfscript": "tokens,milk
add_boob_size,5",
"sound": "Blow1",
"to": "1,2",
"type": "physical",
"visual": "exp,attack
animation,fence_static
self,Buff,CRIMSON"
},
"cow_holy_strike": {
"ID": "cow_holy_strike",
"crit": "15",
"from": "3,4",
"icon": "cow_strike",
"name": "Holy Strike",
"range": "9,12",
"requirements": "min_stat,STR,16
min_stat,CON,15
min_stat,DEX,14",
"script": "token_scaling,milk,-20
save,FOR
move,-2
tokens,stun",
"selfscript": "move,3
tokens,daze
add_boob_size,5",
"sound": "Blow1",
"to": "1",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"cow_holy_touch": {
"ID": "cow_holy_touch",
"crit": "10",
"from": "1,2",
"icon": "cow_love_touch_2",
"name": "Holy Touch",
"range": "6,7",
"requirements": "",
"script": "token_scaling,milk,-20
save,WIL
tokens,exposure",
"selfscript": "move,1
lust,10
tokens,milk
add_boob_size,5",
"sound": "Blow1",
"to": "1,2",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"cow_love_touch": {
"ID": "cow_love_touch",
"crit": "10",
"from": "1,2",
"icon": "cow_love_touch",
"name": "Love Touch",
"range": "5,7",
"requirements": "",
"script": "token_scaling,milk,-15
save,WIL
tokens,exposure",
"selfscript": "tokens,milk
add_boob_size,5",
"sound": "Blow1",
"to": "1,2",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"cow_milkshot": {
"ID": "cow_milkshot",
"crit": "0",
"from": "1,2",
"icon": "milkshot",
"name": "Milkshot",
"range": "2,3",
"requirements": "self_token_count,milk,2",
"script": "token_scaling,milk,100",
"selfscript": "remove_tokens,milk",
"sound": "Water1",
"to": "1,2",
"type": "magic",
"visual": "animation,lactate
exp,attack
projectile,StraightStream,WHITE"
},
"cow_stab": {
"ID": "cow_stab",
"crit": "15",
"from": "1,2",
"icon": "cow_stab",
"name": "Stab",
"range": "7,9",
"requirements": "",
"script": "token_scaling,milk,-25
save,FOR
remove_tokens,block",
"selfscript": "move,1
tokens,daze
token_chance,50,exposure",
"sound": "Slash1",
"to": "1,2",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
}
}