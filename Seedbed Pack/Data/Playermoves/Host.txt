{
"aggregation_pheromone": {
"ID": "aggregation_pheromone",
"crit": "",
"from": "3,4",
"icon": "release_pheromone",
"name": "Aggregation Pheromone",
"range": "",
"requirements": "self_token_count,nectar,2",
"script": "dot,estrus,10,3
tokens,crit,triggered,triggered,triggered",
"selfscript": "spend_tokens,nectar,nectar",
"sound": "Fog1,0.3",
"to": "ally,all",
"type": "none",
"visual": "animation,buff
target,Mist,PINK"
},
"alarm_pheromone": {
"ID": "alarm_pheromone",
"crit": "",
"from": "3,4",
"icon": "alarm_pheromone",
"name": "Alarm Pheromone",
"range": "",
"requirements": "tokens,tentacles",
"script": "tokens,seedbed_alarm",
"selfscript": "",
"sound": "Fire1,0.3",
"to": "ally,all",
"type": "none",
"visual": "exp,attack
animation,cast
target,Mist,PINK"
},
"aphrodisiac_spray": {
"ID": "aphrodisiac_spray",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Tentacles: Aphrodisiac Spray",
"range": "2,4",
"requirements": "",
"script": "ignore_tokens,riposte
token_scaling,tentacles,30
save,REF
dot,love,2,3",
"selfscript": "",
"sound": "Water1",
"to": "1,2",
"type": "none",
"visual": "exp,attack
animation,cast
projectile,Spit,DEEP_PINK"
},
"attractive_pheromone": {
"ID": "attractive_pheromone",
"crit": "",
"from": "3,4",
"icon": "attractive_pheromone",
"name": "Attractive Pheromone",
"range": "",
"requirements": "self_token_count,nectar,2",
"script": "lust,20
tokens,blockminus,blockminus,taunt,taunt",
"selfscript": "spend_tokens,nectar,nectar",
"sound": "Water1",
"to": "ally,1,2,aoe",
"type": "none",
"visual": "exp,round
animation,cast
target,Rain,GOLDENROD"
},
"breastfeed": {
"ID": "breastfeed",
"crit": "10",
"from": "2,3,4",
"icon": "breastfeed",
"name": "Breastfeed",
"range": "3,5",
"requirements": "self_token_count,nectar,1",
"script": "save,WIL
dot,estrus,10,3",
"selfscript": "spend_tokens,nectar",
"sound": "Heal",
"to": "ally,other",
"type": "heal",
"visual": "exp,round
animation,lactate
target,Buff,FOREST_GREEN"
},
"get_some_milk": {
"ID": "get_some_milk",
"crit": "",
"from": "any",
"icon": "get_some_milk",
"name": "Get Some Milk, Cuties.",
"range": "",
"requirements": "tokens,tentacles
self_token_count,nectar,2",
"script": "tokens,tentacles,tentacles",
"selfscript": "spend_tokens,nectar,nectar
lust,20
satisfaction,20",
"sound": "Heal",
"to": "ally",
"type": "none",
"visual": "exp,round
animation,lactate
target,Heal,DEEP_PINK"
},
"host_seduce": {
"ID": "host_seduce",
"crit": "",
"from": "any",
"icon": "host_seduce",
"name": "Seduce",
"range": "",
"requirements": "own_min_lust,50",
"script": "",
"selfscript": "tokens,lc_seduced,lc_seduced,lc_seduced",
"sound": "Skill",
"to": "self",
"type": "none",
"visual": "exp,smirk
area,Color,PINK
animation,lactate
target,Buff,DEEP_PINK"
},
"lc_break_cocoon": {
"ID": "lc_break_cocoon",
"crit": "",
"from": "any",
"icon": "none",
"name": "Living Clothes: Break Cocoon",
"range": "",
"requirements": "tokens,cocoon,latex_cocoon",
"script": "",
"selfscript": "remove_tokens,cocoon,latex_cocoon,lc_annoyed",
"sound": "Fire1",
"to": "self",
"type": "none",
"visual": "exp,attack
animation,buff
self,Mist,PINK"
},
"lc_cum": {
"ID": "lc_cum",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Living Clothes: Cumstream",
"range": "6,12",
"requirements": "",
"script": "ignore_tokens,riposte
save,REF
dot,poison,2,3",
"selfscript": "move,-1",
"sound": "Water1",
"to": "1,2,3,aoe",
"type": "none",
"visual": "animation,straightbow
exp,attack
projectile,StraightStream,PINK"
},
"lc_force_abortion": {
"ID": "lc_force_abortion",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Living Clothes: Force Abortion",
"range": "5,10",
"requirements": "no_target_tokens,impregnation_block",
"script": "remove_parasite
save,FOR
dot,bleed,5,3",
"selfscript": "",
"sound": "Blow1",
"to": "self",
"type": "none",
"visual": "exp,round
area,Color,PINK
animation,cramps"
},
"lc_groping": {
"ID": "lc_groping",
"crit": "",
"from": "any",
"icon": "none",
"name": "Living Clothes: Groping",
"range": "",
"requirements": "",
"script": "",
"selfscript": "satisfaction,20
lust,20
tokens,lc_cum",
"sound": "",
"to": "self",
"type": "none",
"visual": "exp,round
in_place
animation,cramps
target,Buff,DEEP_PINK"
},
"lc_nipple_fuck": {
"ID": "lc_nipple_fuck",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Living Clothes: Nipple Fuck",
"range": "3,5",
"requirements": "",
"script": "",
"selfscript": "satisfaction,20
tokens,nectar,nectar
tokens,lc_cum
dot,estrus,5,2",
"sound": "Heal",
"to": "self",
"type": "heal",
"visual": "exp,round
in_place
animation,lactate
target,Buff,PURPLE"
},
"lc_pump_eggs": {
"ID": "lc_pump_eggs",
"crit": "",
"from": "any",
"icon": "none",
"name": "Living Clothes: Pump Eggs",
"range": "",
"requirements": "",
"script": "",
"selfscript": "lust,50
satisfaction,50
tokens,lc_cum,lc_cum
attach_specific_parasite,tentacles",
"sound": "",
"to": "self",
"type": "none",
"visual": "exp,round
area,Color,PINK
animation,cramps"
},
"lc_pump_fluid": {
"ID": "lc_pump_fluid",
"crit": "",
"from": "any",
"icon": "none",
"name": "Living Mask: Pump Fluid",
"range": "",
"requirements": "",
"script": "",
"selfscript": "satisfaction,20
dot,regen,5,2
dot,estrus,5,2
grow_parasite_with_modifier",
"sound": "Heal",
"to": "self",
"type": "none",
"visual": "in_place
animation,hide
area,Color,PINK
target,Buff,PURPLE"
},
"lc_pump_nutrient": {
"ID": "lc_pump_nutrient",
"crit": "5",
"from": "any",
"icon": "none",
"name": "Living Clothes: Pump Nutrient",
"range": "2,4",
"requirements": "",
"script": "",
"selfscript": "lust,40
satisfaction,20
tokens,lc_cum
grow_parasite_with_modifier",
"sound": "Heal",
"to": "self",
"type": "heal",
"visual": "exp,round
in_place
area,Color,PINK
animation,cramps"
},
"nectar_overflow": {
"ID": "nectar_overflow",
"crit": "20",
"from": "any",
"icon": "none",
"name": "Nectar Overflow",
"range": "1,3",
"requirements": "self_token_count,nectar,4",
"script": "token_scaling,nectar,25
convert_token,tentacles,tentacles,tentacles
save,WIL
dot,estrus,5,2",
"selfscript": "spend_tokens,nectar,nectar,nectar
satisfaction,20
lust,20
grow_parasite_with_modifier",
"sound": "Water1,0.3",
"to": "ally,all",
"type": "heal",
"visual": "exp,round
animation,lactate
target,Rain,DEEP_PINK"
},
"non_host_seduce": {
"ID": "non_host_seduce",
"crit": "",
"from": "any",
"icon": "host_seduce",
"name": "Let's have some fun <3",
"range": "",
"requirements": "own_min_lust,80",
"script": "",
"selfscript": "tokens,lc_seduced_no_seedbed",
"sound": "Skill",
"to": "self",
"type": "none",
"visual": "exp,smirk
area,Color,PINK
animation,lactate
target,Buff,DEEP_PINK"
},
"pheromone_spray": {
"ID": "pheromone_spray",
"crit": "",
"from": "3,4",
"icon": "pheromone_spray",
"name": "Pheromone Spray",
"range": "",
"requirements": "self_token_count,tentacles,1
self_token_count,nectar,2",
"script": "tokens,seedbed_alarm_target
chain_move,alarm_pheromone",
"selfscript": "spend_tokens,nectar,nectar",
"sound": "Water1",
"to": "1,2",
"type": "none",
"visual": "exp,attack
animation,cast
projectile,Spit,GOLDENROD"
},
"poison_spit": {
"ID": "poison_spit",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Tentacles: Poison Spit",
"range": "1,3",
"requirements": "",
"script": "ignore_tokens,riposte
token_scaling,tentacles_barrier,30
save,REF
dot,poison,2,3",
"selfscript": "",
"sound": "Water1",
"to": "1,2",
"type": "none",
"visual": "projectile,Bolt,PURPLE"
},
"poison_spray": {
"ID": "poison_spray",
"crit": "10",
"from": "any",
"icon": "none",
"name": "Tentacles: Poison Spray",
"range": "2,4",
"requirements": "",
"script": "ignore_tokens,riposte
token_scaling,tentacles,30
token_scaling,seedbed_alarm,100
save,REF
dot,poison,2,3",
"selfscript": "remove_tokens,seedbed_alarm",
"sound": "Water1",
"to": "1,2",
"type": "none",
"visual": "exp,attack
animation,cast
projectile,Spit,PURPLE"
},
"protect_me": {
"ID": "protect_me",
"crit": "",
"from": "any",
"icon": "protect_me",
"name": "Protect Me!",
"range": "",
"requirements": "",
"script": "target_guard,2
tokens,riposte,riposte",
"selfscript": "move,-1",
"sound": "Heal",
"to": "ally,other",
"type": "none",
"visual": "in_place
animation,cast
self,Buff,ORANGE
target,Guard,ORANGE"
},
"share_tentacles": {
"ID": "share_tentacles",
"crit": "",
"from": "any",
"icon": "share_tentacles",
"name": "Share Tentacles",
"range": "",
"requirements": "self_token_count,tentacles,2",
"script": "tokens,tentacles,tentacles",
"selfscript": "spend_tokens,tentacles,tentacles",
"sound": "Fog1",
"to": "ally,other",
"type": "none",
"visual": "animation,kiss
target,Mist,PINK"
},
"tentacles_barrier": {
"ID": "tentacles_barrier",
"crit": "",
"from": "3,4",
"icon": "tentacles_barrier",
"name": "Tentacles Barrier",
"range": "",
"requirements": "self_token_count,tentacles,2",
"script": "tokens,tentacles_barrier,tentacles_barrier,tentacles_barrier",
"selfscript": "spend_tokens,tentacles,tentacles",
"sound": "Key",
"to": "ally",
"type": "none",
"visual": "in_place
animation,cast
target,Guard,PURPLE"
}
}