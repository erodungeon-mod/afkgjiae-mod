{
"brisant_bouclier": {
"ID": "brisant_bouclier",
"crit": "30",
"from": "3,4",
"icon": "brisant_bouclier",
"name": "Brisant Bouclier",
"range": "5,6",
"requirements": "",
"script": "ignore_tokens,block
remove_tokens,block
leech,50",
"selfscript": "tokens,taunt,taunt
dot,bleed,2,2
move,3",
"sound": "Slash,0.3",
"to": "2,3,4",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"chant_du_deuliste": {
"ID": "chant_du_deuliste",
"crit": "",
"from": "2,3,4",
"icon": "chant_du_deuliste",
"name": "Chant du Duelliste",
"range": "",
"requirements": "",
"script": "",
"selfscript": "swift
tokens,sublimation
move,1",
"sound": "Skill",
"to": "self",
"type": "none",
"visual": "in_place
animation,cast
self,Buff,PURPLE"
},
"defi": {
"ID": "defi",
"crit": "",
"from": "any",
"icon": "defi",
"name": "Défi",
"range": "",
"requirements": "",
"script": "tokens,riposte,riposte,taunt
chain_move,rosier_sanguin",
"selfscript": "tokens,strength,strength",
"sound": "Skill",
"to": "any",
"type": "none",
"visual": "in_place
animation,cast
target,Buff,PURPLE"
},
"fleur_de_sang": {
"ID": "fleur_de_sang",
"crit": "",
"from": "1,2",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "",
"requirements": "self_token_count,grievous_pain,10",
"script": "",
"selfscript": "tokens,bloodflower,stealth,dodgeplus,dodgeplus",
"sound": "Skill",
"to": "self",
"type": "none",
"visual": "in_place
animation,cast
self,Buff,CRIMSON"
},
"fleur_de_sang_1": {
"ID": "fleur_de_sang_1",
"crit": "15",
"from": "any",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "3,4",
"requirements": "",
"script": "token_scaling,thornblade,20
ignore_tokens,dodge,riposte,gl_counter,enemy_bq_life_link_self
leech,10",
"selfscript": "",
"sound": "Slash1,0.3",
"to": "any",
"type": "none",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"fleur_de_sang_2": {
"ID": "fleur_de_sang_2",
"crit": "30",
"from": "any",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "3,4",
"requirements": "",
"script": "token_scaling,thornblade,30
ignore_tokens,dodge,riposte,gl_counter,enemy_bq_life_link_self
leech,20",
"selfscript": "",
"sound": "Slash2,0.4",
"to": "any",
"type": "none",
"visual": "exp,attack
animation,attack
self,Buff,CRIMSON"
},
"fleur_de_sang_3": {
"ID": "fleur_de_sang_3",
"crit": "45",
"from": "any",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "4,5",
"requirements": "",
"script": "token_scaling,thornblade,40
ignore_tokens,dodge,riposte,gl_counter,enemy_bq_life_link_self
leech,25",
"selfscript": "",
"sound": "Slash3,0.5",
"to": "any",
"type": "none",
"visual": "exp,attack
animation,fence_static
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"fleur_de_sang_4": {
"ID": "fleur_de_sang_4",
"crit": "60",
"from": "any",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "6,8",
"requirements": "",
"script": "token_scaling,thornblade,60
ignore_tokens,dodge,riposte,gl_counter,enemy_bq_life_link_self
leech,50",
"selfscript": "",
"sound": "Slash4,0.6",
"to": "any",
"type": "none",
"visual": "exp,attack
animation,stab
self,Buff,CRIMSON"
},
"fleur_de_sang_5": {
"ID": "fleur_de_sang_5",
"crit": "75",
"from": "any",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"range": "3,5",
"requirements": "",
"script": "token_scaling,thornblade,80
ignore_tokens,dodge,riposte,gl_counter,enemy_bq_life_link_self
leech,10
dot,bleed,4,2",
"selfscript": "remove_tokens,bloodflower,grievous_pain
move,-3
dot,bleed,5,4",
"sound": "Slash9,0.7",
"to": "all",
"type": "none",
"visual": "exp,attack
animation,fence_backward
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"folie_florale": {
"ID": "folie_florale",
"crit": "15",
"from": "1,2",
"icon": "folie_florale",
"name": "Folie Florale",
"range": "3,4",
"requirements": "",
"script": "ignore_tokens,dodge
remove_tokens,dodge
leech,40
dot,bleed,3,2",
"selfscript": "dot,bleed,3,1
move,-1",
"sound": "Slash,0.3",
"to": "3,4,aoe",
"type": "physical",
"visual": "exp,attack
animation,fence_backward
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"lame_sanglante": {
"ID": "lame_sanglante",
"crit": "",
"from": "1,2,3",
"icon": "lame_sanglante",
"name": "Lamé Sanglante",
"range": "",
"requirements": "",
"script": "",
"selfscript": "swift
tokens,thornblade
move,-1",
"sound": "Skill",
"to": "self",
"type": "none",
"visual": "in_place
animation,cast
self,Buff,CRIMSON"
},
"ponction_epineuse": {
"ID": "ponction_epineuse",
"crit": "30",
"from": "4",
"icon": "ponction_epineuse",
"name": "Ponction Épineuse",
"range": "6,7",
"requirements": "",
"script": "ignore_tokens,riposte
remove_tokens,riposte
leech,100
dot,bleed,3,2",
"selfscript": "dot,bleed,2,2
move,2",
"sound": "Slash,0.3",
"to": "1",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
target,Debuff,CRIMSON"
},
"ronces_ecarlates": {
"ID": "ronces_ecarlates",
"crit": "20",
"from": "2,3",
"icon": "ronces_ecarlates",
"name": "Ronces Écarlates",
"range": "3,4",
"requirements": "",
"script": "leech,40
dot,bleed,3,2
save,FOR
tokens,armor_break,armor_break
token_chance,50,armor_break,armor_break",
"selfscript": "tokens,dodge,dodge
dot,bleed,2,2
move,1",
"sound": "Slash,0.3",
"to": "1,2",
"type": "physical",
"visual": "exp,attack
animation,fence_forward
self,Buff,CRIMSON"
},
"rosier_sanguin": {
"ID": "rosier_sanguin",
"crit": "15",
"from": "any",
"icon": "rosier_sanguin",
"name": "Rosier Sanguin",
"range": "3,4",
"requirements": "",
"script": "token_scaling,dodge,40
dot,bleed,2,2",
"selfscript": "tokens,dodgeminus
dot,bleed,1,2",
"sound": "Slash,0.3",
"to": "any",
"type": "physical",
"visual": "exp,attack
animation,fence_static
self,Buff,CRIMSON"
},
"saignee_sylvestre": {
"ID": "saignee_sylvestre",
"crit": "15",
"from": "1",
"icon": "saignee_sylvestre",
"name": "Saignée Sylvestre",
"range": "2,3",
"requirements": "",
"script": "ignore_tokens,dodge
leech,40
dot,bleed,3,2",
"selfscript": "tokens,stealth
dot,bleed,2,2
move,-2",
"sound": "Slash,0.3",
"to": "1,2,aoe",
"type": "physical",
"visual": "exp,attack
animation,fence_static
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"serenade_letale": {
"ID": "serenade_letale",
"crit": "15",
"from": "2,3",
"icon": "serenade_letale",
"name": "Sérénade Létale",
"range": "2,3",
"requirements": "",
"script": "leech,40
dot,bleed,6,2",
"selfscript": "tokens,taunt,taunt,dodgeplus,dodgeplus
dot,bleed,3,2
move,-1",
"sound": "Slash,0.3",
"to": "2,3,aoe",
"type": "physical",
"visual": "exp,attack
animation,fence_backward
self,Buff,CRIMSON
target,Rain,CRIMSON"
}
}