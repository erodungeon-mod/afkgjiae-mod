{
"swordsman_base": {
"ID": "swordsman_base",
"cost": "0",
"flags": "",
"group": "swordsman",
"icon": "swordsman_class",
"name": "Swordsman",
"position": "3,2",
"reqs": "",
"script": "allow_moves,heavy_slash,swordsman_charge,armor_breaking"
},
"swordsman_base_2": {
"ID": "swordsman_base_2",
"cost": "0",
"flags": "",
"group": "swordsman",
"icon": "swordsman_class",
"name": "Slow Attack",
"position": "3,1",
"reqs": "",
"script": "miss,5"
},
"swordsman_expertise": {
"ID": "swordsman_expertise",
"cost": "2",
"flags": "repeat",
"group": "swordsman",
"icon": "swordsman_class",
"name": "Swordsman Expertise",
"position": "3,8",
"reqs": "swordsman_permanent",
"script": "HP,2"
},
"swordsman_left_1": {
"ID": "swordsman_left_1",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "dodge_goal",
"name": "Dash & Swing",
"position": "1,3",
"reqs": "swordsman_mid_1",
"script": "allow_moves,dash_and_swing"
},
"swordsman_left_2": {
"ID": "swordsman_left_2",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "dodge_goal",
"name": "Rushing Strike",
"position": "2,4",
"reqs": "swordsman_left_1",
"script": "allow_moves,rushing_strike"
},
"swordsman_left_3": {
"ID": "swordsman_left_3",
"cost": "2",
"flags": "",
"group": "swordsman",
"icon": "fightobj_goal",
"name": "Battle Cry",
"position": "2,5",
"reqs": "swordsman_left_2",
"script": "WHEN:combat_start
tokens,cast,strength,vuln"
},
"swordsman_left_4": {
"ID": "swordsman_left_4",
"cost": "2",
"flags": "",
"group": "swordsman",
"icon": "dodge_goal",
"name": "Fury Swing",
"position": "2,6",
"reqs": "swordsman_left_3",
"script": "allow_moves,fury_swing"
},
"swordsman_mid_1": {
"ID": "swordsman_mid_1",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "FOR_goal",
"name": "Strong Body",
"position": "3,3",
"reqs": "swordsman_base",
"script": "STR,1"
},
"swordsman_mid_2": {
"ID": "swordsman_mid_2",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "riposte_goal",
"name": "Counter Stance",
"position": "3,4",
"reqs": "swordsman_mid_1",
"script": "allow_moves,counter_stance"
},
"swordsman_mid_3": {
"ID": "swordsman_mid_3",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "damage_goal",
"name": "Titan Roar",
"position": "3,5",
"reqs": "swordsman_mid_2",
"script": "allow_moves,titan_roar"
},
"swordsman_mid_4": {
"ID": "swordsman_mid_4",
"cost": "2",
"flags": "",
"group": "swordsman",
"icon": "fighthard_goal",
"name": "All Out",
"position": "3,6",
"reqs": "swordsman_mid_3",
"script": "allow_moves,all_out"
},
"swordsman_permanent": {
"ID": "swordsman_permanent",
"cost": "3",
"flags": "permanent",
"group": "swordsman",
"icon": "swordsman_class",
"name": "Claymore Master",
"position": "3,7",
"reqs": "swordsman_left_4
swordsman_mid_4
swordsman_right_4",
"script": "STR,1
CON,1"
},
"swordsman_right_1": {
"ID": "swordsman_right_1",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "hurt_goal",
"name": "Hashing",
"position": "5,3",
"reqs": "swordsman_mid_1",
"script": "allow_moves,hashing"
},
"swordsman_right_2": {
"ID": "swordsman_right_2",
"cost": "1",
"flags": "",
"group": "swordsman",
"icon": "bleed_goal",
"name": "Life Steal",
"position": "4,4",
"reqs": "swordsman_right_1",
"script": "allow_moves,life_steal"
},
"swordsman_right_3": {
"ID": "swordsman_right_3",
"cost": "2",
"flags": "",
"group": "swordsman",
"icon": "fight_goal",
"name": "Strong Spirit",
"position": "4,5",
"reqs": "swordsman_right_2",
"script": "WIL,20
STR,1"
},
"swordsman_right_4": {
"ID": "swordsman_right_4",
"cost": "2",
"flags": "",
"group": "swordsman",
"icon": "dodge_goal",
"name": "Tempest",
"position": "4,6",
"reqs": "swordsman_right_3",
"script": "allow_moves,tempest"
}
}