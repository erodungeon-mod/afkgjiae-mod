{
"evil_tail_base": {
"ID": "evil_tail_base",
"cost": "0",
"flags": "",
"group": "grievous_lady",
"icon": "grievous_lady_class",
"name": "Grievous Lady",
"position": "3,1",
"reqs": "",
"script": "alts,Grievous Lady
alts,avoid_danger
adds,grievous_lady_base
require_clothes_tag,Grievous Lady
require_underwear_tag,Grievous Lady
require_extra_tag,Grievous Lady
disable_all_class_change
add_moves,chant_du_deuliste,lame_sanglante
allow_moves,rosier_sanguin
min_desire,boobs,100"
},
"evil_tail_buff": {
"ID": "evil_tail_buff",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "grievous_lady_class",
"name": "Pain Addiction",
"position": "2,6",
"reqs": "evil_tail_mid",
"script": "IF:above_satisfaction,0
recoil,-50
ENDIF
affliction_weight,masochistic,100
affliction_weight,obedient,-200
affliction_weight,oversensitive,-200
affliction_weight,lecherous,-200
affliction_weight,exhibitionist,-200
clearheaded_chance,-100
denied_chance,-100"
},
"evil_tail_debuff": {
"ID": "evil_tail_debuff",
"cost": "0",
"flags": "",
"group": "grievous_lady",
"icon": "bleed_goal",
"name": "Grievous Pain",
"position": "3,9",
"reqs": "",
"script": "force_dot,bleed,3
FOR:token,faltered
DMG,10
ENDFOR
WHEN:self_hit
grow_parasite
ENDWHEN"
},
"evil_tail_effect": {
"ID": "evil_tail_effect",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "grievous_lady_class",
"name": "Love is Pain",
"position": "3,7",
"reqs": "evil_tail_ultimate
evil_tail_buff",
"script": "love_conversion,25
IF:above_satisfaction,0
DMG,50
WHEN:self_hit
IF:NOT:has_token,bloodflower
satisfaction,20
dot,bleed,2,1"
},
"evil_tail_left_1": {
"ID": "evil_tail_left_1",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Ronces Écarlates",
"position": "2,2",
"reqs": "evil_tail_base",
"script": "allow_moves,ronces_ecarlates"
},
"evil_tail_left_2": {
"ID": "evil_tail_left_2",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Brisant Bouclier",
"position": "2,3",
"reqs": "evil_tail_left_1",
"script": "allow_moves,brisant_bouclier"
},
"evil_tail_left_3": {
"ID": "evil_tail_left_3",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Ponction Épineuse",
"position": "2,4",
"reqs": "evil_tail_left_2",
"script": "allow_moves,ponction_epineuse"
},
"evil_tail_mid": {
"ID": "evil_tail_mid",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "strength_goal",
"name": "Défi",
"position": "3,5",
"reqs": "evil_tail_left_3
evil_tail_right_3",
"script": "allow_moves,defi"
},
"evil_tail_repeatable": {
"ID": "evil_tail_repeatable",
"cost": "2",
"flags": "repeat",
"group": "grievous_lady",
"icon": "grievous_lady_class",
"name": "Blood Manipulation",
"position": "3,8",
"reqs": "evil_tail_effect",
"script": "FOR,2
HP,1"
},
"evil_tail_right_1": {
"ID": "evil_tail_right_1",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Saignée Sylvestre",
"position": "4,2",
"reqs": "evil_tail_base",
"script": "allow_moves,saignee_sylvestre"
},
"evil_tail_right_2": {
"ID": "evil_tail_right_2",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Sérénade Létale",
"position": "4,3",
"reqs": "evil_tail_right_1",
"script": "allow_moves,serenade_letale"
},
"evil_tail_right_3": {
"ID": "evil_tail_right_3",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "hurt_goal",
"name": "Folie Florale",
"position": "4,4",
"reqs": "evil_tail_right_2",
"script": "allow_moves,folie_florale"
},
"evil_tail_ultimate": {
"ID": "evil_tail_ultimate",
"cost": "2",
"flags": "",
"group": "grievous_lady",
"icon": "grievous_lady_class",
"name": "Fleur de Sang",
"position": "4,6",
"reqs": "evil_tail_mid",
"script": "IF:token_count,grievous_pain,10
add_moves,fleur_de_sang
ENDIF
WHEN:self_struck
tokens,grievous_pain"
}
}