{
"impregnate_vine": {
"ID": "impregnate_vine",
"crit": "10",
"dur": "",
"from": "any",
"love": "10,20",
"name": "Seeding",
"range": "1,2",
"requirements": "empty_slot,outfit
empty_slot,under
no_target_tokens,impregnation_block,preg_first,preg_second,preg_third",
"script": "save,FOR
tokens,preg_vine",
"selfscript": "",
"sound": "Blow1",
"to": "any",
"type": "physical",
"visual": "animation,attack
target,Mist,PINK"
}
}