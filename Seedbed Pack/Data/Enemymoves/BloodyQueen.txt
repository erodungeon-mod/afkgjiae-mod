{
"bq_alarm_pheromone": {
"ID": "bq_alarm_pheromone",
"crit": "",
"dur": "",
"from": "any",
"love": "",
"name": "Alarm Pheromone",
"range": "",
"requirements": "free_enemy_space",
"script": "",
"selfscript": "swift
add_enemy,evil_tail_mature",
"sound": "Pollen",
"to": "self",
"type": "none",
"visual": "in_place
animation,buff
self,Mist,PINK"
},
"bq_assaut_ecarlate": {
"ID": "bq_assaut_ecarlate",
"crit": "30",
"dur": "1",
"from": "2,3",
"love": "",
"name": "Assaut Écarlate",
"range": "6,9",
"requirements": "",
"script": "ignore_tokens,dodge,riposte
dot,bleed,4,2
save,FOR
tokens,stun
move,-3",
"selfscript": "move,1",
"sound": "Sword4",
"to": "1,2,aoe",
"type": "physical",
"visual": "animation,mod/bloodyqueen_cross_slash
self,Buff,CRIMSON
target,Explosion,CRIMSON"
},
"bq_bloody_transform": {
"ID": "bq_bloody_transform",
"crit": "",
"dur": "",
"from": "any",
"love": "",
"name": "Unleash Power",
"range": "",
"requirements": "",
"script": "",
"selfscript": "transform,bloody_queen_ultimate",
"sound": "Up4",
"to": "self",
"type": "none",
"visual": "animation,buff
self,Mist,CRIMSON
exp,attack"
},
"bq_bloodyqueen_riposte": {
"ID": "bq_bloodyqueen_riposte",
"crit": "",
"dur": "",
"from": "any",
"love": "",
"name": "",
"range": "",
"requirements": "",
"script": "ignore_defensive_tokens
tokens,bq_target",
"selfscript": "tokens,bq_revenge",
"sound": "Skill",
"to": "any",
"type": "none",
"visual": "target,Debuff,CRIMSON"
},
"bq_eclair": {
"ID": "bq_eclair",
"crit": "",
"dur": "",
"from": "any",
"love": "",
"name": "Éclair",
"range": "",
"requirements": "",
"script": "guard,1
swap_with_target",
"selfscript": "swift
tokens,sublimation",
"sound": "Skill",
"to": "ally,other",
"type": "none",
"visual": "self,Buff,CRIMSON
in_place
immediate"
},
"bq_eclair_pourpre": {
"ID": "bq_eclair_pourpre",
"crit": "40",
"dur": "1",
"from": "2,3",
"love": "",
"name": "Éclair Pourpre",
"range": "7,12",
"requirements": "",
"script": "ignore_tokens,dodge
dot,bleed,3,2",
"selfscript": "tokens,dodgeplus,dodgeplus,strength,strength
move,2",
"sound": "Attack3",
"to": "2,3,4",
"type": "physical",
"visual": "animation,mod/bloodyqueen_cross_slash
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_entaille_croisee": {
"ID": "bq_entaille_croisee",
"crit": "15",
"dur": "1",
"from": "1,2",
"love": "",
"name": "Entaille Croisée",
"range": "3,5",
"requirements": "",
"script": "leech,80
dot,bleed,2,2",
"selfscript": "",
"sound": "Slash1",
"to": "1,2",
"type": "physical",
"visual": "animation,mod/bloodyqueen_cross_slash
self,Buff,CRIMSON"
},
"bq_entaille_profonde": {
"ID": "bq_entaille_profonde",
"crit": "10",
"dur": "1",
"from": "1,2,3",
"love": "",
"name": "Entaille Profonde",
"range": "2,4",
"requirements": "",
"script": "dot,bleed,4,2
save,FOR
tokens,grievous_wound",
"selfscript": "tokens,dodge
move,-1",
"sound": "Slash1",
"to": "1,2",
"type": "physical",
"visual": "animation,mod/bloodyqueen_slash_1
self,Buff,CRIMSON"
},
"bq_estocade": {
"ID": "bq_estocade",
"crit": "30",
"dur": "1",
"from": "2,3,4",
"love": "",
"name": "Estocade",
"range": "4,6",
"requirements": "",
"script": "dot,bleed,2,2
save,FOR
tokens,vuln",
"selfscript": "move,1",
"sound": "Slash10",
"to": "1,2",
"type": "physical",
"visual": "animation,mod/bloodyqueen_thrust_2
self,Buff,CRIMSON"
},
"bq_flux_sanguin": {
"ID": "bq_flux_sanguin",
"crit": "0",
"dur": "0",
"from": "any",
"love": "",
"name": "Flux Sanguin",
"range": "2,5",
"requirements": "",
"script": "ignore_tokens,riposte
leech,50
save,FOR
dot,bleed,2,2",
"selfscript": "",
"sound": "Absorb1",
"to": "all",
"type": "magic",
"visual": "target,Mist,CRIMSON
self,Buff,CRIMSON"
},
"bq_lien_vital": {
"ID": "bq_lien_vital",
"crit": "5",
"dur": "1",
"from": "any",
"love": "",
"name": "Lien Vital",
"range": "2,5",
"requirements": "no_enemy_tokens,bq_life_link",
"script": "tokens,bq_life_link,bq_life_link,bq_life_link,bq_life_link,bq_life_link",
"selfscript": "tokens,enemy_bq_life_link_self,enemy_bq_life_link_self,enemy_bq_life_link_self,enemy_bq_life_link_self,enemy_bq_life_link_self",
"sound": "Slash1",
"to": "any",
"type": "magic",
"visual": "animation,mod/bloodyqueen_reverse_slash_1
self,Buff,CRIMSON
target,Debuff,CRIMSON"
},
"bq_lien_vital_effect": {
"ID": "bq_lien_vital_effect",
"crit": "10",
"dur": "0",
"from": "any",
"love": "",
"name": "Lien Vital",
"range": "4,6",
"requirements": "tokens,bq_life_link",
"script": "ignore_defensive_tokens
ignore_tokens,riposte
leech,100
dot,bleed,2,2
spend_tokens,bq_life_link",
"selfscript": "spend_tokens,enemy_bq_life_link_self",
"sound": "Absorb1",
"to": "all",
"type": "magic",
"visual": "animation,buff
self,Buff,CRIMSON
target,Mist,CRIMSON"
},
"bq_rafale_sanguine": {
"ID": "bq_rafale_sanguine",
"crit": "35",
"dur": "1",
"from": "any",
"love": "",
"name": "Rafale Sanguine",
"range": "12,16",
"requirements": "dots,bleed",
"script": "leech,100
remove_dots,bleed",
"selfscript": "",
"sound": "Water2",
"to": "any",
"type": "magic",
"visual": "animation,cast
target,Explosion,CRIMSON
self,Buff,CRIMSON"
},
"bq_rosier_sanguin": {
"ID": "bq_rosier_sanguin",
"crit": "25",
"dur": "1",
"from": "any",
"love": "",
"name": "Rosier Sanguin",
"range": "7,10",
"requirements": "",
"script": "leech,100
dot,bleed,4,2
tokens,vuln,vuln",
"selfscript": "tokens,dodge,dodge",
"sound": "Slash1",
"to": "any",
"type": "physical",
"visual": "animation,mod/bloodyqueen_slash_2
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_ruee_sanguine": {
"ID": "bq_ruee_sanguine",
"crit": "40",
"dur": "1",
"from": "3,4",
"love": "",
"name": "Ruée Sanguine",
"range": "10,15",
"requirements": "",
"script": "ignore_tokens,block
remove_tokens,block
dot,bleed,3,2
save,FOR
tokens,armor_break,armor_break,armor_break
token_chance,40,armor_break,armor_break",
"selfscript": "tokens,taunt,taunt,blockminus,blockminus
move,3",
"sound": "Slash11",
"to": "1,2",
"type": "physical",
"visual": "animation,mod/bloodyqueen_double_thrust
self,Buff,CRIMSON
target,Explosion,CRIMSON"
},
"bq_saignee_fatale": {
"ID": "bq_saignee_fatale",
"crit": "20",
"dur": "1",
"from": "1,2",
"love": "",
"name": "Saignée Fatale",
"range": "4,9",
"requirements": "",
"script": "dot,bleed,6,2
save,FOR
dot,bleed,3,5",
"selfscript": "",
"sound": "Slash1",
"to": "2,3,aoe",
"type": "physical",
"visual": "animation,mod/bloodyqueen_slash_2
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_tempete_ecarlate": {
"ID": "bq_tempete_ecarlate",
"crit": "30",
"dur": "1",
"from": "1,2",
"love": "",
"name": "Tempête Écarlate",
"range": "8,10",
"requirements": "",
"script": "dot,bleed,4,2
save,FOR
tokens,weakness,weakness,armor_break,armor_break,armor_break",
"selfscript": "move,-1",
"sound": "Wind1",
"to": "2,3,4,aoe",
"type": "magic",
"visual": "animation,mod/bloodyqueen_reverse_slash_2
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_vengeance_ecarlate_1": {
"ID": "bq_vengeance_ecarlate_1",
"crit": "",
"dur": "1",
"from": "any",
"love": "",
"name": "Vengeance Écarlate",
"range": "2,4",
"requirements": "tokens,bq_target",
"script": "token_scaling,bq_revenge,15
leech,40
ignore_tokens,riposte",
"selfscript": "",
"sound": "Slash1",
"to": "all",
"type": "physical",
"visual": "animation,mod/bloodyqueen_thrust_2
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_vengeance_ecarlate_2": {
"ID": "bq_vengeance_ecarlate_2",
"crit": "",
"dur": "1",
"from": "any",
"love": "",
"name": "Vengeance Écarlate",
"range": "2,5",
"requirements": "tokens,bq_target",
"script": "token_scaling,bq_revenge,15
leech,40
ignore_tokens,riposte",
"selfscript": "",
"sound": "Slash2",
"to": "all",
"type": "physical",
"visual": "animation,mod/bloodyqueen_reverse_slash_1
self,Buff,CRIMSON
target,Rain,CRIMSON"
},
"bq_vengeance_ecarlate_3": {
"ID": "bq_vengeance_ecarlate_3",
"crit": "",
"dur": "1",
"from": "any",
"love": "",
"name": "Vengeance Écarlate",
"range": "3,5",
"requirements": "tokens,bq_target",
"script": "token_scaling,bq_revenge,15
leech,40
dot,bleed,3,2
remove_tokens,bq_target
ignore_tokens,riposte",
"selfscript": "remove_tokens,bq_revenge",
"sound": "Slash3",
"to": "all",
"type": "physical",
"visual": "animation,mod/bloodyqueen_cross_slash
self,Buff,CRIMSON
target,Rain,CRIMSON"
}
}