{
"all_out": {
"ID": "all_out",
"icon": "all_out_token",
"name": "All Out",
"script": "alter_move,all_out,cancel_all_out
mulphyDMG,40
mulphyREC,25
max_hp,-25
WHEN:turn
convert_token,block,strength",
"types": "special",
"usage": "limit,1"
},
"armor_break": {
"ID": "armor_break",
"icon": "armor_break_token",
"name": "Pressing",
"script": "FOR:token,armor_break
REC,10
ENDFOR
WHEN:turn
remove_tokens,armor_break",
"types": "special",
"usage": "limit,4
counter,barrier"
},
"barrier": {
"ID": "barrier",
"icon": "barrier_token",
"name": "Barrier",
"script": "FOR:token,barrier
REC,-10
ENDFOR
WHEN:turn
remove_tokens,barrier",
"types": "special",
"usage": "limit,7
counter,armor_break"
},
"blockminus": {
"ID": "blockminus",
"icon": "blockminus_token",
"name": "Block-",
"script": "mulREC,-25",
"types": "positive",
"usage": "time,defence
prefer,blockplus,block
alt,block
turn,3
limit,3
counter,vuln"
},
"bloodflower": {
"ID": "bloodflower",
"icon": "fleur_de_sang",
"name": "Fleur de Sang",
"script": "recoil,-75
WHEN:turn_end
free_action,fleur_de_sang_1
free_action,fleur_de_sang_2
free_action,fleur_de_sang_3
free_action,fleur_de_sang_4
free_action,fleur_de_sang_5",
"types": "hidden",
"usage": "limit,1"
},
"boob_balance": {
"ID": "boob_balance",
"icon": "boob_balance",
"name": "Size Balance",
"script": "REC,20
loveREC,50
IF:body_type,boobs4
phyDMG,5
ENDIF
IF:body_type,boobs5
phyDMG,15
ENDIF
IF:body_type,boobs6
phyDMG,30",
"types": "special",
"usage": ""
},
"bq_bloody_dance": {
"ID": "bq_bloody_dance",
"icon": "sanglant_danse_token",
"name": "Sanglant Danse",
"script": "WHEN:turn_end
free_action,sanglant_danse_1
free_action,sanglant_danse_2
free_action,sanglant_danse_3
free_action,sanglant_danse_4
free_action,sanglant_danse_5",
"types": "hidden",
"usage": "limit,1"
},
"bq_bloody_lance": {
"ID": "bq_bloody_lance",
"icon": "bloody_lance",
"name": "Bloody Worm's mucus",
"script": "FOR:token,bq_bloody_lance
saves,-10",
"types": "hidden",
"usage": "limit,4
turn,3"
},
"bq_cross_slash": {
"ID": "bq_cross_slash",
"icon": "entaille_croisee",
"name": "Entaille Croisée",
"script": "FOR:token,bq_cross_slash
mulDMG,10",
"types": "hidden",
"usage": "limit,2
turn,4"
},
"bq_crown": {
"ID": "bq_crown",
"icon": "couronne_de_sang",
"name": "Couronne de Sang",
"script": "adds,bloody_queen_crown_buff
mulDMG,10
leech,25
force_dot,bleed,3",
"types": "hidden",
"usage": "limit,1
turn,3"
},
"bq_deep_cut": {
"ID": "bq_deep_cut",
"icon": "entaille_profonde",
"name": "Entaille Profonde",
"script": "FOR:token,bq_deep_cut
leech,8",
"types": "hidden",
"usage": "limit,2
turn,4"
},
"bq_frenzied_blade": {
"ID": "bq_frenzied_blade",
"icon": "lame_frenetique",
"name": "Lame Frénétique",
"script": "WHEN:turn_end
free_action,lame_frenetique_1
free_action,lame_frenetique_2
free_action,lame_frenetique_3",
"types": "hidden",
"usage": "limit,1"
},
"bq_life_link": {
"ID": "bq_life_link",
"icon": "lien_vital",
"name": "Lien Vital",
"script": "",
"types": "hidden",
"usage": "limit,5
turn,3"
},
"bq_life_link_self": {
"ID": "bq_life_link_self",
"icon": "lien_vital",
"name": "Lien Vital",
"script": "WHEN:self_struck
free_action,lien_vital_effect",
"types": "hidden",
"usage": "limit,5
turn,3"
},
"bq_love_overflow": {
"ID": "bq_love_overflow",
"icon": "love_overflow_token",
"name": "Love Overflow",
"script": "DMG,25
leech,10
WHEN:turn_end
dot,estrus,20,1
IF:NOT:has_alt,in Ultimate Form
grow_parasite",
"types": "hidden",
"usage": "limit,1
turn,2"
},
"bq_revenge": {
"ID": "bq_revenge",
"icon": "vengeance_ecarlate_token",
"name": "Vengeance Écarlate",
"script": "",
"types": "hidden",
"usage": "limit,6"
},
"bq_target": {
"ID": "bq_target",
"icon": "vengeance_ecarlate",
"name": "Vengeance Écarlate",
"script": "description,Being targeted by Bloody Queen.",
"types": "hidden",
"usage": "limit,1
turn,3"
},
"bq_thrust": {
"ID": "bq_thrust",
"icon": "estocade",
"name": "Estocade",
"script": "FOR:token,bq_thrust
crit,10",
"types": "hidden",
"usage": "limit,2
turn,4"
},
"cast": {
"ID": "cast",
"icon": "cast_token",
"name": "Charge",
"script": "",
"types": "special",
"usage": "turn,10
limit,4"
},
"counter_stance": {
"ID": "counter_stance",
"icon": "counter_stance_token",
"name": "Counter Stance",
"script": "riposte
mulREC,-50
WHEN:self_hit
clear_tokens,counter_stance",
"types": "special",
"usage": "time,defence
turn,3
limit,1"
},
"freeze": {
"ID": "freeze",
"icon": "freeze_token",
"name": "Freeze",
"script": "mulREC,50
dodge,-200
WHEN:turn
skip",
"types": "hidden",
"usage": "time,defence
limit,1
turn,4"
},
"game_over": {
"ID": "game_over",
"icon": "game_over_token",
"name": "Game Over",
"script": "force_dot,regen,99
WHEN:turn
skip",
"types": "hidden",
"usage": "limit,1"
},
"grievous_pain": {
"ID": "grievous_pain",
"icon": "birth_evil_tail_token",
"name": "Grievous Pain",
"script": "",
"types": "hidden",
"usage": "limit,10
dungeon"
},
"grievous_wound": {
"ID": "grievous_wound",
"icon": "grievous_wound_token",
"name": "Grievous Wound",
"script": "healREC,-50",
"types": "negative",
"usage": "limit,1
turn,3"
},
"madoka_1": {
"ID": "madoka_1",
"icon": "soul_pink_token",
"name": "Soul Flame",
"script": "WHEN:enemy_hit
dot,fire,5,2
ENDWHEN
WHEN:self_hit
dot,cleanse,5,2
ENDWHEN",
"types": "hidden",
"usage": "limit,1
dungeon"
},
"nectar": {
"ID": "nectar",
"icon": "nectar_token",
"name": "Nectar",
"script": "FOR:token,nectar
loveREC,10
ENDFOR
WHEN:turn
add_boob_size,5",
"types": "special",
"usage": "limit,7
dungeon"
},
"parasite_fluid": {
"ID": "parasite_fluid",
"icon": "parasite_fluid_token",
"name": "Parasite Fluid",
"script": "WHEN:round
IF:has_parasite,parasite
IF:chance,40
free_action,wriggling_parasite
ENDIF
ELSE:
remove_tokens,parasite_fluid",
"types": "hidden",
"usage": "limit,6"
},
"restraint": {
"ID": "restraint",
"icon": "restraint_token",
"name": "Restraint",
"script": "SPD,-4
dodge,-50",
"types": "negative",
"usage": "time,defence
prefer,restraintplus
turn,3
limit,3
counter,dodge"
},
"restraintminus": {
"ID": "restraintminus",
"icon": "restraintminus_token",
"name": "Restraint-",
"script": "SPD,-2
dodge,-25",
"types": "negative",
"usage": "time,defence
prefer,restraintplus,restraint
alt,restraint
turn,3
limit,3
counter,dodge"
},
"restraintplus": {
"ID": "restraintplus",
"icon": "restraintplus_token",
"name": "Restraint+",
"script": "SPD,-6
dodge,-75",
"types": "negative",
"usage": "time,defence
alt,restraint
turn,3
limit,3
counter,dodge"
},
"seedbed_alarm": {
"ID": "seedbed_alarm",
"icon": "alarm_pheromone_token",
"name": "Alarm Pheromone",
"script": "",
"types": "hidden",
"usage": "turn,2
limit,1"
},
"seedbed_alarm_target": {
"ID": "seedbed_alarm_target",
"icon": "alarm_pheromone_target_token",
"name": "Alarm Pheromone",
"script": "taunt",
"types": "hidden",
"usage": "turn,2
limit,1"
},
"slime_fluid": {
"ID": "slime_fluid",
"icon": "slime_fluid_token",
"name": "Slime Fluid",
"script": "FOR:token,slime_fluid
phyREC,-5
magREC,5",
"types": "special",
"usage": "time,offence
limit,4
turn,5"
},
"sublimation": {
"ID": "sublimation",
"icon": "sublimation",
"name": "Sublimation",
"script": "mulDMG,50",
"types": "hidden",
"usage": "turn,1
limit,1"
},
"tentacles": {
"ID": "tentacles",
"icon": "parasites_token",
"name": "Tentacles",
"script": "adds,tentacles
WHEN:round
IF:no_tokens,cocoon
IF:has_token,seedbed_alarm
free_action,poison_spray
ELSE:
IF:chance,50
free_action,aphrodisiac_spray
ENDIF
ENDIF
ENDIF
ENDWHEN
WHEN:self_struck
IF:chance,30
remove_tokens,tentacles
ENDWHEN",
"types": "hidden",
"usage": "limit,7
dungeon"
},
"tentacles_barrier": {
"ID": "tentacles_barrier",
"icon": "bad_parasites_token",
"name": "Tentacles Barrier",
"script": "adds,tentacles_barrier
FOR:token,tentacles_barrier
REC,-10
ENDFOR
WHEN:self_struck
free_action,poison_spit
IF:chance,20
remove_tokens,tentacles_barrier
ENDWHEN",
"types": "hidden",
"usage": "limit,4
dungeon"
},
"thorn": {
"ID": "thorn",
"icon": "thorn_token",
"name": "Thorns",
"script": "FOR:token,thorn
mulphyDMG,5",
"types": "hidden",
"usage": "dungeon
limit,7"
},
"thorn_used": {
"ID": "thorn_used",
"icon": "thorn_empty_token",
"name": "Thorns (Used)",
"script": "FOR:token,thorn_used
dodge,3
ENDFOR
WHEN:turn
convert_token,thorn_used,thorn",
"types": "hidden",
"usage": "dungeon
limit,7"
},
"thornblade": {
"ID": "thornblade",
"icon": "thornblade",
"name": "Lame Sanglante",
"script": "WHEN:enemy_hit
IF:NOT:has_token,bloodflower
dot,bleed,2,2
ENDIF
ENDWHEN
WHEN:self_hit
IF:NOT:has_token,bloodflower
dot,regen,5,2
ENDIF",
"types": "hidden",
"usage": "turn,1
limit,1"
},
"triggered": {
"ID": "triggered",
"icon": "triggered_token",
"name": "Triggered",
"script": "FOR:token,triggered
DMG,10
ENDFOR
WHEN:turn_end
remove_tokens,triggered",
"types": "special",
"usage": "limit,7"
}
}