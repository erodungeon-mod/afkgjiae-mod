{
"lc_annoyed": {
"ID": "lc_annoyed",
"icon": "lc_annoyed_token",
"name": "*Living Clothes is annoyed!",
"script": "WHEN:round
IF:has_token,cocoon,latex_cocoon
IF:token_count,lc_annoyed,2
free_action,lc_break_cocoon
ENDIF
ELSE:
remove_tokens,lc_annoyed",
"types": "hidden",
"usage": "limit,2"
},
"lc_breeding": {
"ID": "lc_breeding",
"icon": "lc_breeding_token",
"name": "*Living Clothes wants to breed ",
"script": "WHEN:round
IF:has_token,preg_first,preg_second,preg_third
free_action,lc_pump_nutrient
remove_tokens,lc_breeding
ELSE:
IF:NOT:has_token,impregnation_block
free_action,lc_pump_eggs
remove_tokens,lc_breeding
ENDIF
ENDIF
ENDWHEN",
"types": "hidden",
"usage": "limit,1"
},
"lc_cum": {
"ID": "lc_cum",
"icon": "lc_attacking_token",
"name": "*Living Clothes is gonna cum ",
"script": "WHEN:round
IF:NOT:has_token,cocoon,latex_cocoon
IF:token_count,lc_cum,4
free_action,lc_cum
clear_tokens,lc_cum
ENDIF
ELSE:
tokens,lc_annoyed
ENDIF
ENDWHEN",
"types": "hidden",
"usage": "limit,4
dungeon"
},
"lc_groping": {
"ID": "lc_groping",
"icon": "lc_groping_token",
"name": "*Living Clothes wants to grope its host ",
"script": "WHEN:round
free_action,lc_groping
remove_tokens,lc_groping",
"types": "hidden",
"usage": "limit,1"
},
"lc_nipple_fuck": {
"ID": "lc_nipple_fuck",
"icon": "lc_nipple_fuck_token",
"name": "*Living Clothes wants to fuck its host's nipples ",
"script": "WHEN:round
free_action,lc_nipple_fuck
remove_tokens,lc_nipple_fuck",
"types": "hidden",
"usage": "limit,1"
},
"lc_seduced": {
"ID": "lc_seduced",
"icon": "seduce_token",
"name": "*Living Clothes is excited!",
"script": "loveREC,100
WHEN:round
remove_tokens,lc_seduced",
"types": "hidden",
"usage": "limit,4
dungeon"
},
"lc_seduced_no_seedbed": {
"ID": "lc_seduced_no_seedbed",
"icon": "seduce_token",
"name": "*Living Clothes is excited!",
"script": "WHEN:round
IF:NOT:has_token,lc_groping,lc_nipple_fuck,lc_breeding
random_token,lc_groping,lc_groping,lc_nipple_fuck,lc_breeding
ENDIF
ENDIF
remove_tokens,lc_seduced_no_seedbed",
"types": "hidden",
"usage": "limit,4
dungeon"
}
}